module Main where

import Lib
import qualified Network.Socket as N  -- network

myServer = "irc.netwichtig.de" :: String
myPort   = 6667 :: N.PortNumber
myChannels   = ["#nais-testing", "#nais"] :: [String]
myNick   = "nais" :: String

main :: IO ()
main = runBirch myServer myPort myNick myChannels
